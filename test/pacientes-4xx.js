'use strict';

// Load modules

const Jwt = require('jsonwebtoken');
const Code = require('@hapi/code');
const Lab = require('@hapi/lab');
const Server = require('../server');

// Test shortcuts

const { describe, it } = exports.lab = Lab.script();
const { expect } = Code;

// Utils
const { randomBytes } = require('crypto');

/**
* Returns a random integer between min (inclusive) and max (inclusive).
* The value is no lower than min (or the next integer greater than min
* if min isn't an integer) and no greater than max (or the next integer
* lower than max if max isn't an integer).
* Using Math.round() will give you a non-uniform distribution!
*/
const randomInt = (min, max) => {

    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

describe('Familia de pruebas unitarias para errores 4xx en /pacientes/{paciente}', () => {

    const token = Jwt.sign({ sub: 1, scope: ['admin'] }, 'allsafe');

    it('[GET] /pacientes/{paciente} responde 400 cuando {paciente} no es un id válido', async () => {

        const server = await Server.deployment();

        // Genero un id {paciente} de 4 caracteres, cuidando que NO sea número positivo
        let paciente;
        do {
            paciente = randomBytes(4).toString('hex');
        }
        while (!isNaN(paciente) || Number(paciente) > 0);

        // Pido el {paciente}; NOTA: {paciente} no es válido por no ser número entero positivo
        const citas = await server.inject({
            method: 'get',
            url: `/pacientes/${paciente}`
        });

        expect(citas.statusCode).to.equal(400);
    });

    it('[PATCH] /pacientes/{paciente} responde 400 cuando {paciente} no es un id válido', async () => {

        const server = await Server.deployment();

        // Genero un id {paciente} de 4 caracteres, cuidando que NO sea número positivo
        let paciente;
        do {
            paciente = randomBytes(4).toString('hex');
        }
        while (!isNaN(paciente) || Number(paciente) > 0);

        // Pido el {paciente}; NOTA: {paciente} no es válido por no ser número entero positivo
        const citas = await server.inject({
            method: 'patch',
            url: `/pacientes/${paciente}`,
            payload: {
                apellido: 'Suárez'
            }
        });

        expect(citas.statusCode).to.equal(400);
    });

    it('[DELETE] /pacientes/{paciente} responde 400 cuando {paciente} no es un id válido', async () => {

        const server = await Server.deployment();

        // Genero un id {paciente} de 4 caracteres, cuidando que NO sea número positivo
        let paciente;
        do {
            paciente = randomBytes(4).toString('hex');
        }
        while (!isNaN(paciente) || Number(paciente) > 0);

        // Pido el {paciente}; NOTA: {paciente} no es válido por no ser número entero positivo
        const citas = await server.inject({
            method: 'delete',
            url: `/pacientes/${paciente}`
        });

        expect(citas.statusCode).to.equal(400);
    });

    it('[GET] /pacientes/{paciente} responde 404 cuando {paciente} no existe en la BD', async () => {

        const server = await Server.deployment();

        // Genero un id {paciente} muy grande; tan grande que no está en la BD
        const paciente = randomInt(9000000, 1000000);
        // Pido todas las citas de {paciente}; NOTA: {paciente} sí es válido, pero no existe
        const citas = await server.inject({
            method: 'get',
            url: `/pacientes/${paciente}`,
            headers: { authorization: `Bearer ${token}` }
        });

        expect(citas.statusCode).to.equal(404);
    });

    it('[PATCH] /pacientes/{paciente} responde 404 cuando {paciente} no existe en la BD', async () => {

        const server = await Server.deployment();

        // Genero un id {paciente} muy grande; tan grande que no está en la BD
        const paciente = randomInt(9000000, 1000000);
        // Pido todas las citas de {paciente}; NOTA: {paciente} sí es válido, pero no existe
        const citas = await server.inject({
            method: 'patch',
            url: `/pacientes/${paciente}`,
            headers: { authorization: `Bearer ${token}` },
            payload: {
                apellido: 'Suárez'
            }
        });

        expect(citas.statusCode).to.equal(404);
    });

    it('[DELETE] /pacientes/{paciente} responde 404 cuando {paciente} no existe en la BD', async () => {

        const server = await Server.deployment();

        // Genero un id {paciente} muy grande; tan grande que no está en la BD
        const paciente = randomInt(9000000, 1000000);
        // Pido todas las citas de {paciente}; NOTA: {paciente} sí es válido, pero no existe
        const citas = await server.inject({
            method: 'delete',
            url: `/pacientes/${paciente}`,
            headers: { authorization: `Bearer ${token}` }
        });

        expect(citas.statusCode).to.equal(404);
    });
});
