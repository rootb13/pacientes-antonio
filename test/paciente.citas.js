'use strict';

// Load modules

const Jwt = require('jsonwebtoken');
const Code = require('@hapi/code');
const Lab = require('@hapi/lab');
const Server = require('../server');

// Test shortcuts

const { describe, it } = exports.lab = Lab.script();
const { expect } = Code;

// Utils
const { randomBytes } = require('crypto');

/**
* Returns a random integer between min (inclusive) and max (inclusive).
* The value is no lower than min (or the next integer greater than min
* if min isn't an integer) and no greater than max (or the next integer
* lower than max if max isn't an integer).
* Using Math.round() will give you a non-uniform distribution!
*/
const randomInt = (min, max) => {

    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

describe('Familia de pruebas unitarias para las citas de un paciente en específico', () => {

    let server;

    let token;

    it('[GET] /pacientes/{paciente}/citas devuelve la lista de citas del {paciente}', async () => {

        server = await Server.deployment();

        await server.models().Paciente
            .query()
            .where({ RFC: 'PDPJR88888888' })
            .delete();

        const { id: paciente } = await server.models().Paciente
            .query()
            .insertAndFetch({
                nombre: 'Paciente de Pruebas',
                apellido: 'Jimenez Rodriguez',
                telefono: '8888888888',
                correo: 'jimenez_rodriguez@pacientes.modl.dev',
                rfc: 'PDPJR88888888'
            });

        token = Jwt.sign({ sub: 1, scope: ['administrador'] }, 'allsafe');

        await server.models().Cita
            .query()
            .delete()
            .where({ paciente });

        await server.models().Cita
            .query()
            .insertAndFetch({
                paciente,
                medico: 2,
                inicio: 1563906015389,
                fin: 1563906031934
            });

        // Pido todas las citas de {paciente}
        const citas = await server.inject({
            method: 'get',
            url: `/pacientes/${paciente}/citas`,
            headers: {
                authorization: `Bearer ${token}`
            }
        });


        // Borro las citas de prueba de la BD
        await server.models().Cita
            .query()
            .where({ paciente })
            .delete();

        // Borro el paciente de la BD
        await server.models().Paciente
            .query()
            .where({ id: paciente })
            .delete();

        expect(citas.statusCode).to.equal(200);
        expect(citas.result).to.be.array();
        expect(citas.result).to.not.be.empty();
        for (const cita of citas.result) {
            expect(cita).to.be.an.object();
            expect(cita.id).to.be.a.number();
            expect(cita.id).to.be.above(0);

            expect(cita.paciente).to.be.a.number();
            expect(Number(cita.paciente)).to.equal(Number(paciente));
            expect(cita.medico).to.be.a.number();
            expect(cita.inicio).to.be.a.number();
            expect(cita.fin).to.be.a.number();
            expect(cita.inicio).to.be.a.number();
        }
    });

    it('[GET] /pacientes/{paciente}/citas responde 400 cuando {paciente} no es un id válido', async () => {

        // Genero un id {paciente} de 4 caracteres, cuidando que NO sea número positivo
        let paciente;
        do {
            paciente = randomBytes(4).toString('hex');
        }
        while (!isNaN(paciente) || Number(paciente) > 0);

        // Pido todas las citas de {paciente}; NOTA: {paciente} no es válido por no ser número entero positivo
        const citas = await server.inject({
            method: 'get',
            url: `/pacientes/${paciente}/citas`
        });

        expect(citas.statusCode).to.equal(400);
    });

    it('[GET] /pacientes/{paciente}/citas responde 404 cuando {paciente} no existe en la BD', async () => {

        // Genero un id {paciente} muy grande; tan grande que no está en la BD
        const paciente = randomInt(9000000, 1000000);
        // Pido todas las citas de {paciente}; NOTA: {paciente} sí es válido, pero no existe
        const citas = await server.inject({
            method: 'get',
            url: `/pacientes/${paciente}/citas`,
            headers: {
                authorization: `Bearer ${token}`
            }
        });

        expect(citas.statusCode).to.equal(404);
    });

});
