'use strict';

// Load modules

const Jwt = require('jsonwebtoken');
const Code = require('@hapi/code');
const Lab = require('@hapi/lab');
const Server = require('../server');

// Test shortcuts

const { describe, it } = exports.lab = Lab.script();
const { expect } = Code;

describe('Pruebas para Citas', () => {

    let citaId;

    let server;

    const token = Jwt.sign({ sub: 1, scope: ['admin'] }, 'allsafe');

    it('[POST] /citas.', async () => {

        server = await Server.deployment();

        await server.models()
            .Cita
            .query()
            .delete();

        const cita = await server.inject({
            method: 'post',
            url: '/citas',
            payload: {
                paciente: 1,
                medico: 2,
                inicio: 1565017200000,
                fin: 1565019000000
            },
            headers: { authorization: `Bearer ${token}` }
        });

        citaId = cita.result.id;

        expect(cita.statusCode).to.equal(200);
        expect(cita.result).to.be.an.object();
        expect(cita.result.id).to.be.a.number();
        expect(cita.result.paciente).to.be.a.number();
        expect(cita.result.medico).to.be.a.number();
        expect(cita.result.inicio).to.be.a.number();
        expect(cita.result.fin).to.be.a.number();

    });

    it('[POST] /citas no admite citas duplicadas por medico en servicio', async () => {

        const cita = await server.inject({
            method: 'post',
            url: '/citas',
            payload: {
                paciente: 8,
                medico: 2,
                inicio: 1565017200000,
                fin: 1565019000000
            }
        });

        expect(cita.statusCode).to.equal(400);

    });

    it('[GET] /citas/{cita} funciona', async () => {

        const citas = await server.inject({
            method: 'get',
            url: `/citas/${citaId}`,
            headers: { authorization: `Bearer ${token}` }
        });

        expect(citas.statusCode).to.equal(200);
        const cita = citas.result;

        expect(cita).to.be.an.object();
        expect(cita.id).to.be.a.number();
        expect(cita.id).to.equal(citaId);
        expect(cita.paciente).to.be.a.number();
        expect(cita.medico).to.be.a.number();
        expect(cita.inicio).to.be.a.number();
        expect(cita.fin).to.be.a.number();

    });

    it('[GET] /citas funciona.', async () => {

        const citas = await server.inject({
            method: 'get',
            url: '/citas',
            headers: { authorization: `Bearer ${token}` }
        });

        expect(citas.statusCode).to.equal(200);
        expect(citas.result).to.be.array();
        expect(citas.result).to.not.be.empty();
        for (const cita of citas.result) {
            expect(cita).to.be.an.object();
            expect(cita.id).to.be.a.number();
            expect(cita.id).to.be.above(0);

            expect(cita.paciente).to.be.a.number();
            expect(cita.medico).to.be.a.number();
        }

    });

    it('[PATCH] /citas/{cita}.', async () => {

        const cita = await server.inject({
            method: 'patch',
            url: `/citas/${citaId}`,
            payload: {
                paciente: 22,
                medico: 2,
                inicio: 1570438800000,
                fin: 1570442400000
            },
            headers: { authorization: `Bearer ${token}` }
        });

        expect(cita.statusCode).to.equal(200);
        expect(cita.result).to.be.an.object();
        expect(cita.result.id).to.be.a.number();
        expect(cita.result.paciente).to.be.a.number();
        expect(cita.result.medico).to.be.a.number();
        expect(cita.result.inicio).to.be.a.number();
        expect(cita.result.fin).to.be.a.number();

    });

    it('[DELETE] /citas/{cita}.', async () => {

        const cita = await server.inject({
            method: 'delete',
            url: `/citas/${citaId}`,
            headers: { authorization: `Bearer ${token}` }
        });

        const deleted = await server.models()
            .Cita
            .query()
            .where({ id: citaId })
            .delete();

        expect(cita.statusCode).to.equal(200);
        expect(deleted).to.equal(0);

        await server.models()
            .Cita
            .query()
            .delete();

    });
});
