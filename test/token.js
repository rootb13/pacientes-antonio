'use strict';

// Load modules

const Code = require('@hapi/code');
const Lab = require('@hapi/lab');
const Server = require('../server');

// Test shortcuts

const { describe, it } = exports.lab = Lab.script();
const { expect } = Code;

describe('Deployment', () => {

    let correo;

    let contraseña;

    let credentialUtf8;

    let credentialBase64;

    let limipiarBaseDatos;

    let server;

    it('Se da de alta un usuario y se codifica en base64 para mandarlo en el header auth basic', async () => {

        server = await Server.deployment();

        limipiarBaseDatos = await server.models()
            .Usuario
            .query()
            .delete();

        const altaUsuario = await server.models()
            .Usuario
            .query()
            .insert({
                nombre: 'Antonio',
                apellido: 'Token',
                correo: 'antonio@token.com',
                contraseña: 'nuñezmañana',
                rol: 'paramedico'
            });

        correo = altaUsuario.correo;

        contraseña = altaUsuario.contraseña;

        credentialUtf8 = correo + ':' + contraseña;

        credentialBase64 = Buffer.from(credentialUtf8, 'utf-8').toString('base64');

        // Authorization: Basic dXNlcjpwYXNz
        const acceso = await server.inject({
            method: 'get',
            url: '/tokens/cuenta',
            headers: {
                authorization: `Basic ${credentialBase64}`
            }
        });

        expect(acceso.statusCode).to.equal(200);
        expect(acceso.result).to.be.an.object();
        expect(acceso.result.token).to.be.a.string();

    });

    it('Error 400 cuando no mando el header (vacio) sin Authorization Basic.', async () => {

        const headerSinAuthorization = await server.inject({
            method: 'get',
            url: '/tokens/cuenta',
            headers: {}
        });

        expect(headerSinAuthorization.statusCode).to.equal(400);
    });

    it('Error 400 envio el header Authorization Vasic con un formato invalido.', async () => {

        const headerAV = await server.inject({
            method: 'get',
            url: '/tokens/cuenta',
            headers: {
                authorization: `Vasic ${credentialBase64}`
            }
        });

        expect(headerAV.statusCode).to.equal(400);
    });

    it('Error 400 envio el header Authorization Basic sin credencial.', async () => {

        const headerLessCredential = await server.inject({
            method: 'get',
            url: '/tokens/cuenta',
            headers: {
                authorization: `Basic `
            }
        });

        expect(headerLessCredential.statusCode).to.equal(400);
    });

    it('Error 401 envio el header Authorization Basic con un correo inexistente en base de datos.', async () => {

        const credentialCI = 'correonoregistrado@hotmail.com' + ':' + contraseña;

        const credentialBase64CI = Buffer.from(credentialCI, 'utf-8').toString('base64');

        const headerCGE = await server.inject({
            method: 'get',
            url: '/tokens/cuenta',
            headers: {
                authorization: `Basic ${credentialBase64CI}`
            }
        });

        expect(headerCGE.statusCode).to.equal(401);

    });

    it('Error 401 envio el header Authorization Basic sin correo.', async () => {

        const credentialSC = ':' + contraseña;

        const credentialBase64SC = Buffer.from(credentialSC, 'utf-8').toString('base64');

        const headerCSC = await server.inject({
            method: 'get',
            url: '/tokens/cuenta',
            headers: {
                authorization: `Basic ${credentialBase64SC}`
            }
        });

        expect(headerCSC.statusCode).to.equal(401);

    });

    it('Error 401 envio el header Authorization Basic algo que no es un correo (un nombre o frase).', async () => {

        const credentialCI = 'no soy un correo' + ':' + contraseña;

        const credentialBase64CInvalid = Buffer.from(credentialCI, 'utf-8').toString('base64');

        const headerCI = await server.inject({
            method: 'get',
            url: '/tokens/cuenta',
            headers: {
                authorization: `Basic ${credentialBase64CInvalid}`
            }
        });

        expect(headerCI.statusCode).to.equal(401);

    });

    it('Error 401 cuando el correo es correcto pero la contraseña no.', async () => {

        const credentialPI = correo + ':' + 'noescorrecta';

        const credentialBase64PI = Buffer.from(credentialPI, 'utf-8').toString('base64');

        const authorizationIncorrect = await server.inject({
            method: 'get',
            url: '/tokens/cuenta',
            headers: {
                authorization: `Basic ${credentialBase64PI}`
            }
        });

        expect(authorizationIncorrect.statusCode).to.equal(401);

    });


    it('Error 401 envio el header Authorization Basic sin contraseña.', async () => {

        const credentialPIncorrect = correo + ':';

        const credentialBase64PIncorrect = Buffer.from(credentialPIncorrect, 'utf-8').toString('base64');

        const authorizationSC = await server.inject({
            method: 'get',
            url: '/tokens/cuenta',
            headers: {
                authorization: `Basic ${credentialBase64PIncorrect}`
            }
        });

        expect(authorizationSC.statusCode).to.equal(401);

        limipiarBaseDatos;

    });

});
