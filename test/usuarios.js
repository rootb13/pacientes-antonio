'use strict';

// Load modules

const Jwt = require('jsonwebtoken');
const Code = require('@hapi/code');
const Lab = require('@hapi/lab');
const Server = require('../server');

// Test shortcuts

const { describe, it } = exports.lab = Lab.script();
const { expect } = Code;

describe('Pruebas para Usuarios', () => {

    let server;

    const token = Jwt.sign({ sub: 1, scope: ['admin'] }, 'allsafe');

    let usuarioId;

    it('[POST] /usuarios.', async () => {

        server = await Server.deployment();

        await server.models()
            .Usuario
            .query()
            .delete();

        const usuario = await server.inject({
            method: 'post',
            url: '/usuarios',
            payload: {
                nombre: 'Antonio',
                apellido: 'Post',
                correo: 'prueba@email.com',
                contraseña: 'contraseña',
                rol: 'Contador'
            },
            headers: { authorization: `Bearer ${token}` }
        });

        usuarioId = usuario.result.id;

        expect(usuario.statusCode).to.equal(200);
        expect(usuario.result).to.be.an.object();
        expect(usuario.result.id).to.be.a.number();
        expect(usuario.result.nombre).to.be.a.string();
        expect(usuario.result.apellido).to.be.a.string();
        expect(usuario.result.correo).to.be.a.string();
        expect(usuario.result.contraseña).to.not.exist();
        expect(usuario.result.rol).to.be.a.string();

    });

    it('[POST] /usuarios no admite usuarios con correo duplicado.', async () => {

        const usuarioDuplicado = await server.inject({
            method: 'post',
            url: '/usuarios',
            payload: {
                nombre: 'Antonio',
                apellido: 'Correo duplicado',
                correo: 'prueba@email.com',
                contraseña: 'ondodoncia',
                rol: 'Dentista'
            }
        });

        expect(usuarioDuplicado.statusCode).to.equal(400);

    });

    it('[GET] /usuarios/{usuario} funciona.', async () => {

        const usuarios = await server.inject({
            method: 'get',
            url: `/usuarios/${usuarioId}`,
            headers: { authorization: `Bearer ${token}` }
        });

        expect(usuarios.statusCode).to.equal(200);
        const usuario = usuarios.result;

        expect(usuario).to.be.an.object();
        expect(usuario.id).to.be.a.number();
        expect(usuario.id).to.be.equal(usuarioId);
        expect(usuario.nombre).to.be.a.string();
        expect(usuario.apellido).to.be.a.string();
        expect(usuario.correo).to.be.a.string();
        expect(usuario.contraseña).to.not.exist();
        expect(usuario.rol).to.be.a.string();

    });

    it('[GET] /usuarios funciona.', async () => {

        const usuarios = await server.inject({
            method: 'get',
            url: '/usuarios',
            headers: { authorization: `Bearer ${token}` }
        });

        expect(usuarios.statusCode).to.equal(200);
        expect(usuarios.result).to.be.array();
        expect(usuarios.result).to.not.be.empty();
        for (const usuario of usuarios.result) {
            expect(usuario).to.be.an.object();
            expect(usuario.id).to.be.a.number();
            expect(usuario.id).to.be.above(0);

            expect(usuario.nombre).to.be.a.string();
            expect(usuario.apellido).to.be.a.string();
            expect(usuario.correo).to.be.a.string();
            expect(usuario.contraseña).to.not.exist();
            expect(usuario.rol).to.be.a.string();
        }

    });

    it('[PATCH] /{usuario}.', async () => {

        const usuario = await server.inject({
            method: 'patch',
            url: `/usuarios/${usuarioId}`,
            payload: {
                nombre: 'Antonio',
                apellido: 'Actualizado',
                correo: 'elliot@ecorp.com',
                contraseña: 'ñiño,ñiña',
                rol: 'Doctor'
            },
            headers: { authorization: `Bearer ${token}` }
        });

        expect(usuario.statusCode).to.equal(200);
        expect(usuario.result).to.be.an.object();
        expect(usuario.result.id).to.be.a.number();
        expect(usuario.result.nombre).to.be.a.string();
        expect(usuario.result.apellido).to.be.a.string();
        expect(usuario.result.correo).to.be.a.string();
        expect(usuario.result.contraseña).to.not.exist();
        expect(usuario.result.rol).to.be.a.string();

    });

    it('[DELETE] /{usuario}.', async () => {

        const usuario = await server.inject({
            method: 'delete',
            url: `/usuarios/${usuarioId}`,
            headers: { authorization: `Bearer ${token}` }
        });

        const deleted = await server.models()
            .Usuario
            .query()
            .where({ id: usuarioId })
            .delete();

        expect(usuario.statusCode).to.equal(200);
        expect(deleted).to.equal(0);

        await server.models()
            .Usuario
            .query()
            .delete();

    });

});
