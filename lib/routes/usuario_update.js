'use strict';

const Joi = require('@hapi/joi');

module.exports = {
    method: 'patch',
    path: '/usuarios/{usuario}',
    options: {
        validate: {
            params: {
                usuario: Joi.number().integer().positive()
            },
            payload: {
                nombre: Joi.string().min(3).max(35),
                apellido: Joi.string().min(3).max(45),
                correo: Joi.string().min(10).max(50),
                contraseña: Joi.string().min(8).max(32),
                rol: Joi.string().min(5).max(15)
            },
            headers: Joi.object({
                authorization: Joi.string().required().regex(new RegExp('^([Bb]earer )[\\w-.~+\/=]{1,}'))
            }).options({ allowUnknown: true })
        }
    },
    handler: async (request, h) => {

        const idU = request.params.usuario;

        const actualizacion = request.payload;

        const usuarioConIdP = await request.models()
            .Usuario
            .query()
            .where({ id: idU })
            .first();

        if (!usuarioConIdP) {
            return h.response({
                statusCode: 404,
                error: 'Not found',
                message: `El usuario ${idU} no existe en la base de datos.`
            }).code(404);
        }

        await request.models()
            .Usuario
            .query()
            .where({ id: idU })
            .patch(actualizacion);

        return { ...usuarioConIdP, contraseña: undefined };
    }
};
