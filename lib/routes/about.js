'use strict';

module.exports = {
    method: 'get',
    path: '/about',
    options: {
        handler: (request, h) => {

            return {
                app: 'pacientes-antonio',
                autor: 'Antonio',
                version: '1.0.0'
            };
        }
    }
};
