'use strict';

const Joi = require('@hapi/joi');

module.exports = {
    method: 'get',
    path: '/citas/{cita}',
    options: {
        validate: {
            params: {
                cita: Joi.number().integer().positive()
            },
            headers: Joi.object({
                authorization: Joi.string().required().regex(new RegExp('^([Bb]earer )[\\w-.~+\/=]{1,}'))
            }).options({ allowUnknown: true })
        }
    },
    handler: async (request, h) => {

        const { cita: id } = request.params;
        const cita = await request.models()
            .Cita
            .query()
            .where({ id })
            .first();

        if (!cita) {
            return h.response({
                statusCode: 404,
                error: 'Not Found',
                message: `La cita ${id} no existe en la base de datos.`
            }).code(404);
        }

        return cita;
    }
};
