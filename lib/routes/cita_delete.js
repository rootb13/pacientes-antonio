'use strict';

const Joi = require('@hapi/joi');

module.exports = {
    method: 'delete',
    path: '/citas/{cita}',
    options: {
        validate: {
            params: {
                cita: Joi.number().integer().positive()
            },
            headers: Joi.object({
                authorization: Joi.string().required().regex(new RegExp('^([Bb]earer )[\\w-.~+\/=]{1,}'))
            }).options({ allowUnknown: true })
        }
    },
    handler: async (request, h) => {

        const idP = request.params.cita;

        const citaConIdP = await request.models()
            .Cita
            .query()
            .where({ id: idP })
            .first();

        if (!citaConIdP) {
            return h.response({
                statusCode: 404,
                error: 'Not Found',
                message: `La cita ${idP} no existe en la base de datos.`
            }).code(404);
        }

        const deleteCita = await request.models()
            .Cita
            .query()
            .where({ id: idP })
            .delete();

        return `${deleteCita} cita ha sido eliminada.`;
    }
};
