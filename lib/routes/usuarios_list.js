'use strict';

const Joi = require('@hapi/joi');

module.exports = {
    method: 'get',
    path: '/usuarios',
    options: {
        validate: {
            headers: Joi.object({
                authorization: Joi.string().required().regex(new RegExp('^([Bb]earer )[\\w-.~+\/=]{1,}'))
            }).options({ allowUnknown: true })
        }
    },
    handler: async (request) => {

        return (await request.models()
            .Usuario
            .query())
            .map((usuario) => ({ ...usuario, contraseña: undefined }));
    }

};
