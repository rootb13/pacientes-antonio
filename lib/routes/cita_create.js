'use strict';

const Joi = require('@hapi/joi');

module.exports = {
    method: 'post',
    path: '/citas',
    options: {
        validate: {
            payload: {
                paciente: Joi.number().integer().positive().min(1),
                medico: Joi.number().integer().positive().min(1),
                inicio: Joi.number().integer().positive().min(1),
                fin: Joi.number().integer().positive().min(1)
            },
            headers: Joi.object({
                authorization: Joi.string().required().regex(new RegExp('^([Bb]earer )[\\w-.~+\/=]{1,}'))
            }).options({ allowUnknown: true })
        }
    },
    handler: async (request, h) => {

        const cita = request.payload;

        const citaConEseMedico = await request.models()
            .Cita
            .query()
            .where({ medico: cita.medico, inicio: cita.inicio, fin: cita.fin });

        if (citaConEseMedico.length > 0) {
            return h.response({ Error: 'Ya hay una cita con este medico, selecciona otro horario disponible' }).code(400);
        }

        const nuevaCita = await request.models()
            .Cita
            .query()
            .insert(cita);

        return nuevaCita;
    }
};
