'use strict';

const Joi = require('@hapi/joi');

module.exports = {
    method: 'get',
    path: '/usuarios/{usuario}',
    options: {
        validate: {
            params: {
                usuario: Joi.number().integer().positive().example(124)
            },
            headers: Joi.object({
                authorization: Joi.string().required().regex(new RegExp('^([Bb]earer )[\\w-.~+\/=]{1,}'))
            }).options({ allowUnknown: true })
        }
    },
    handler: async (request, h) => {

        const { usuario: id } = request.params;
        const usuario = await request.models()
            .Usuario
            .query()
            .where({ id })
            .first();

        if (!usuario) {
            return h.response({
                statusCode: 404,
                error: 'Not Found',
                message: `El usuario ${id} no existe en la base de datos.`
            }).code(404);
        }

        return { ...usuario, contraseña: undefined };
    }
};
