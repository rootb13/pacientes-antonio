'use strict';

const Joi = require('@hapi/joi');

module.exports = {
    method: 'get',
    path: '/pacientes/{paciente}/citas',
    options: {
        validate: {
            params: {
                paciente: Joi.number().integer().positive()
            }
        }
    },
    handler: async (request, h) => {

        const pacienteId = request.params.paciente;

        const pacienteConIdP = await request.models()
            .Paciente
            .query()
            .where({ id: pacienteId })
            .first();

        if (!pacienteConIdP) {
            return h.response({
                statusCode: 404,
                error: 'Not Found',
                message: `El paciente ${pacienteId} no existe en la base de datos.`
            }).code(404);
        }

        return await request.models()
            .Cita
            .query()
            .where({ paciente: pacienteId });
    }
};
