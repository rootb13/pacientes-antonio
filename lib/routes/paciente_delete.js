'use strict';

const Joi = require('@hapi/joi');

module.exports = {
    method: 'delete',
    path: '/pacientes/{paciente}',
    options: {
        validate: {
            params: {
                paciente: Joi.number().integer().positive().min(1)
            },
            headers: Joi.object({
                authorization: Joi.string().required().regex(new RegExp('^([Bb]earer )[\\w-.~+\/=]{1,}'))
            }).options({ allowUnknown: true })
        }
    },
    handler: async (request, h) => {

        const idP = request.params.paciente;

        const pacienteConIdP = await request.models()
            .Paciente
            .query()
            .where({ id: idP })
            .first();

        if (!pacienteConIdP) {
            return h.response({
                statusCode: 404,
                error: 'Not Found',
                message: `El paciente ${idP} no existe en la base de datos.`
            }).code(404);
        }

        const deletePaciente = await request.models()
            .Paciente
            .query()
            .where({ id: idP })
            .delete();

        return `${deletePaciente} paciente se han eliminado.`;
    }
};
