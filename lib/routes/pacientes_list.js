'use strict';

const Joi = require('@hapi/joi');

module.exports = {
    method: 'get',
    path: '/pacientes',
    options: {
        validate: {
            headers: Joi.object({
                authorization: Joi.string().required().regex(new RegExp('^([Bb]earer )[\\w-.~+\/=]{1,}'))
            }).options({ allowUnknown: true })
        }
    },
    handler: async (request, h) => {

        const listaPacientes = await request.models()
            .Paciente
            .query();

        return listaPacientes;
    }
};
