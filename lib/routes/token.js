'use strict';

const Jwt = require('jsonwebtoken');
const Joi = require('@hapi/joi');

module.exports = {
    method: 'get',
    path: '/tokens/cuenta',
    options: {
        validate: {
            headers: Joi.object({
                authorization: Joi.string().required().regex(new RegExp('^([Bb]asic )[\\w-.~+\/=]{1,}'))
            }).options({ allowUnknown: true })
        }
    },
    handler: async (request, h) => {

        const basicHeaders = request.headers.authorization;

        const base64Credentials = basicHeaders.split(' ')[1];

        const credentials = Buffer.from(base64Credentials, 'base64').toString('utf8');

        const [correo, contraseña] = credentials.split(':');

        if (!contraseña) {
            return h.response({
                statusCode: 401,
                error: 'Unauthorized',
                message: 'El correo o contraseña es incorrecto.'
            }).code(401);
        }

        const usuarioRegistrado = await request.models()
            .Usuario
            .query()
            .where({ correo, contraseña })
            .first();

        if (!usuarioRegistrado) {
            return h.response({
                statusCode: 401,
                error: 'Unauthorized',
                message: 'El correo o contraseña es incorrecto'
            }).code(401);
        }

        return { token: (Jwt.sign({ sub: usuarioRegistrado.id, scope: [usuarioRegistrado.rol] }, 'allsafe')) };

    }
};
