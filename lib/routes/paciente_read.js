'use strict';

const Joi = require('@hapi/joi');

module.exports = {
    method: 'get',
    path: '/pacientes/{paciente}',
    options: {
        validate: {
            params: {
                paciente: Joi.number().integer().positive()
            },
            headers: Joi.object({
                authorization: Joi.string().required().regex(new RegExp('^([Bb]earer )[\\w-.~+\/=]{1,}'))
            }).options({ allowUnknown: true })
        }
    },
    handler: async (request, h) => {

        const { paciente: id } = request.params;
        const paciente = await request.models()
            .Paciente
            .query()
            .where({ id })
            .first();

        if (!paciente) {
            return h.response({
                statusCode: 404,
                error: 'Not Found',
                message: `El paciente ${id} no esta en la base de datos.`
            }).code(404);
        }

        return paciente;

    }
};
