'use strict';

const Schwifty = require('schwifty');
const Joi = require('@hapi/joi');

module.exports = class Usuario extends Schwifty.Model {

    static get tableName() {

        return 'usuarios';
    }

    static get joiSchema() {

        return Joi.object({
            id: Joi.number().integer().positive().min(1),
            nombre: Joi.string().min(3).max(35),
            apellido: Joi.string().min(3).max(45),
            correo: Joi.string().min(10).max(50),
            contraseña: Joi.string().min(8).max(32),
            rol: Joi.string().min(5).max(15)
        });
    }
};
