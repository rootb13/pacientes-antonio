'use strict';

const Joi = require('@hapi/joi');
const { Model } = require('./helpers');

module.exports = class Paciente extends Model {

    static get tableName() {

        return 'Pacientes';
    }

    static get joiSchema() {

        return Joi.object({
            id: Joi.number().integer().positive().min(1),
            nombre: Joi.string().min(3).max(35),
            apellido: Joi.string().min(3).max(45),
            telefono: Joi.number().integer().min(1000000000).max(9999999999),
            correo: Joi.string().min(10).max(50),
            rfc: Joi.string().min(10).max(13)
        });
    }
};
