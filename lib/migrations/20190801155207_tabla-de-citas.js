'use strict';

exports.up = async (knex) => {

    await knex.schema.createTable('Citas', (t) => {

        t.increments('id').primary();
        t.biginteger('paciente').index();
        t.biginteger('medico').index();
        t.biginteger('inicio');
        t.biginteger('fin');
    });
};

exports.down = async (knex) => {

    await knex.schema.dropTable('');
};
