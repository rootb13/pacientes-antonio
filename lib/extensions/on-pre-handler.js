/* eslint-disable curly */
'use strict';

const Jwt = require('jsonwebtoken');

module.exports = [{
    method: (request, h) => {

        const { usuario = false, cita = false, paciente = false } = request.params;
        // si no me piden recursos privados, esta extensiòn no hace nada
        if (!usuario && !cita && !paciente) return h.continue;

        const { authorization } = request.headers;

        if (!authorization) {
            return h.response({
                statusCode: 400,
                error: 'Bad Request',
                message: 'Syntax error'
            }).code(400).takeover();
        }

        const token = authorization.split(' ')[1];
        const partesToken = token.split('.');

        if (partesToken.length !== 3) {
            return h.response({
                statusCode: 400,
                error: 'Bad Request',
                message: 'Syntax error'
            }).code(400).takeover();
        }

        try {
            // guardo las identidad del usuario
            request.auth.credentials = Jwt.verify(token, 'allsafe');
            return h.continue;
        }
        catch (err) {
            return h.response({
                statusCode: 401,
                error: 'Unauthorized',
                message: 'Las credenciales proporcinadas son invalidas.'
            }).code(401).takeover();
        }
    }
},
{
    method: (request, h) => {

        const { usuario = false } = request.params;
        if (!usuario) return h.continue;

        const { sub, scope } = request.auth.credentials;

        if (scope.includes('admin') || scope.includes('administrador')) return h.continue;
        if (Number(sub) === Number(usuario)) return h.continue;

        return h.response({
            statusCode: 403,
            error: 'Forbidden',
            message: 'You shall not pass!'
        }).code(403).takeover();
    }
},
{
    method: (request, h) => {

        const metodoHttp = request.method;
        const { paciente = false } = request.params;
        if (!paciente) return h.continue;

        const { scope } = request.auth.credentials;
        if (scope.includes('secretaria') && ['post', 'get', 'patch'].includes(metodoHttp)) return h.continue;
        if (scope.includes('admin') || scope.includes('administrador')) return h.continue;

        return h.response({
            statusCode: 403,
            error: 'Forbidden',
            message: 'You shall not pass!'
        }).code(403).takeover();
    }
},
{
    method: (request, h) => {

        const metodoHttp = request.method;
        const { cita = false } = request.params;
        if (!cita) return h.continue;

        const { scope } = request.auth.credentials;

        if (scope.includes('enfermera') && ('get').includes(metodoHttp)) return h.continue;
        if (scope.includes('secretaria') && ['post', 'get', 'patch'].includes(metodoHttp)) return h.continue;
        if (scope.includes('admin') || scope.includes('administrador')) return h.continue;

        return h.response({
            statusCode: 403,
            error: 'Forbidden',
            message: 'You shall not pass!'
        }).code(403).takeover();
    }
}];
